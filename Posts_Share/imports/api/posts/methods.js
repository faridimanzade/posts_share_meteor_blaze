import {Posts} from "./collections";


Meteor.methods({
    addPost(data){
        return Posts.insert(data);
    },
    removePost(_id){
        return Posts.remove({_id:_id});
    },
    updatePost(_id,checked){
        return Posts.update({_id},{$set:{isPrivate: checked}})
    }
});
