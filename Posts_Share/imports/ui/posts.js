import './posts.html'
import {
    Posts
} from "../api/posts/collections";


Template.posts.onCreated(function () {
    this.autorun(() => {
        this.subscribe('getPosts')
    })
})


Template.posts.helpers({
    allPosts() {
        return Posts.find({
            isPrivate: false
        });
    },
    loggedIn() {
        return Meteor.userId()
    }
});


Template.posts.events({
    'click .deleteBtnPublic'(event, template) {
        Meteor.call('removePost', this._id, (err, res) => {
            if (err) console.log(err)
            if (res) console.log(res)
        })
    },
    'change .updatePublic'(event, template) {
        let _id = this._id;

        Meteor.call('updatePost', _id, !this.isPrivate, (err, res) => {
            if (err) console.log(err)
            if (res) console.log(res)
        })
    }
});