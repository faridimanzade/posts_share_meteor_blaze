import './form.html'



Template.postForm.events({
    'submit #newPost'(e, temp) {
        e.preventDefault();
        let target = e.target;
        let post = target.post.value;
        let isPrivate = target.makePrivate.checked;


        let data = {
            text: post,
            isPrivate: isPrivate,
            userId: Meteor.userId()
        }

        if (data.text) {
            Meteor.call('addPost',data,(err,res)=>{
                if(err) console.log(err)
                if(res) console.log(res)
            })
        }
        

        target.post.value = '';
        target.makePrivate.checked = false;
    }
})