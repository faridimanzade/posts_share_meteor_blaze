import './userPosts.html'
import {
    Posts
} from "../api/posts/collections";


Template.userPosts.onCreated(function () {
    this.autorun(() => {
        this.subscribe('getPosts')
    })
})


Template.userPosts.helpers({
    allPrivatePosts() {
        return Posts.find({
            isPrivate: true,
            userId: Meteor.userId()
        });
    }
});

Template.userPosts.events({
    'click .deleteBtn'(event, template) {
        Meteor.call('removePost', this._id, (err, res) => {
            if (err) console.log(err)
            if (res) console.log(res)
        })
    },
    'change .updatePrivate'(event, template) {
        let _id = this._id;

        Meteor.call('updatePost', _id, !this.isPrivate, (err, res) => {
            if (err) console.log(err)
            if (res) console.log(res)
        })
    }
});